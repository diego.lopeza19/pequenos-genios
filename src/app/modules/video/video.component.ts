import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.sass']
})
export class VideoComponent implements OnInit {

  display: boolean = true;
  titleHeader: string = 'VIDEO';
  srcYoutube;

  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
    private domSanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.utilitiesConfig.classLayout = ['cursor-default'];
    const urlPrefi = this.router.url.replace("/games/", "");
    console.log("urlPrefi", urlPrefi);

    switch (urlPrefi) {
      case "patito-feo":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/qh4f9vhZUTg");
        this.titleHeader = "Cuento"
        break;
      case "rapunzel":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/lnkaXRhOdjs");
        this.titleHeader = "Cuento"
        break;
      case "pinocho":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/pZMuNWiNa0o");
        this.titleHeader = "Cuento"
        break;
      case "blanca-nieves":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/-0nb4g0R9qk");
        this.titleHeader = "Cuento"
        break;
      case "caperucita-roja":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/ZKWX9qAS-Bg");
        this.titleHeader = "Cuento"
        break;
      case "cerditos":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/ViRUaVU9Fzc");
        this.titleHeader = "Cuento"
        break;
      case "cenicienta":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/_6op0AVktr4");
        this.titleHeader = "Cuento"
        break;
      case "gato-con-botas":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/zz6Ckg8SURE");
        this.titleHeader = "Cuento"
        break;
      case "liebre-tortuga":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/js1lKmt-Mag");
        this.titleHeader = "Cuento"
        break;
      case "raton-leon":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/yMSEjiGpuTo");
        this.titleHeader = "Cuento"
        break;
      case "dientes":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/lQN2l-Rswgo");
        this.titleHeader = "Música"
        break;
      case "sapito":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/N-io7Gk_UdE");
        this.titleHeader = "Música"
        break;
      case "auto-papa":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/D_ABTW7aCcs");
        this.titleHeader = "Música"
        break;
      case "taza":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/71pNmDmQklI");
        this.titleHeader = "Música"
        break;
      case "estrellita":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/PyQxnhqj42w");
        this.titleHeader = "Música"
        break;
      case "asereje":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/9tTb0x_uYHU");
        this.titleHeader = "Música"
        break;
      case "aplaudir":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/-HdxrHF-pPE");
        this.titleHeader = "Música"
        break;
      case "animales":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/-yhtDDg7iyw");
        this.titleHeader = "Música"
        break;
      case "pin-pon":
        this.srcYoutube = this.domSanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/Va7RKJ4C6xI");
        this.titleHeader = "Música"
        break;
    }
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }

}
