import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VideoComponent } from './video.component';

const ROUTES: Routes = [{ path: '', component: VideoComponent }];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule
  ],
  exports: [RouterModule]
})
export class VideoRoutingModule { }
