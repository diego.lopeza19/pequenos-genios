import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';
import { Svg } from '@shared/utilities/svg';

@Component({
  selector: 'app-paint',
  templateUrl: './paint.component.html'
})
export class PaintComponent implements OnInit {


  display: boolean = true;
  sidebar: boolean = false;
  loadSvg: any;
  titleHeader: string = 'sandia';
  sidebarIcon: any = 'pi pi-chevron-left';
  selectedColor: any[] = [
    { color: 'Blanco', value: 'white' }
  ];
  arrayColor: any[] = [
    { range: '', color: 'negro', value: 'black' },
    { range: 'light', color: 'plata', value: 'silver' },
    { range: '', color: 'gris', value: 'gray' },
    { range: 'light', color: 'blanco', value: 'white' },
    { range: '', color: 'granate', value: 'maroon' },
    { range: '', color: 'rojo', value: 'red' },
    { range: '', color: 'púrpura', value: 'purple' },
    { range: '', color: 'fucsia', value: 'fuchsia' },
    { range: '', color: 'verde', value: 'green' },
    { range: 'light', color: 'Lima', value: 'lime' },
    { range: '', color: 'aceituna', value: 'olive' },
    { range: 'light', color: 'amarillo', value: 'yellow' },
    { range: '', color: 'Armada', value: 'navy' },
    { range: '', color: 'azul', value: 'blue' },
    { range: '', color: 'verde azulado', value: 'teal' },
    { range: 'light', color: 'agua', value: 'aqua' },
    { range: 'light', color: 'naranja', value: 'orange' },
    { range: 'light', color: 'Alice azul', value: 'aliceblue' },
    { range: 'light', color: 'blanco antiguo', value: 'antiquewhite' },
    { range: 'light', color: 'aguamarina', value: 'aquamarine' },
    { range: 'light', color: 'azur', value: 'azure' },
    { range: 'light', color: 'beige', value: 'beige' },
    { range: 'light', color: 'sopa de mariscos', value: 'bisque' },
    { range: 'light', color: 'blanchedalmond', value: 'blanchedalmond' },
    { range: '', color: 'Violeta Azul', value: 'blueviolet' },
    { range: '', color: 'marrón', value: 'brown' },
    { range: 'light', color: 'burlywood', value: 'burlywood' },
    { range: '', color: 'cadete azul', value: 'cadetblue' },
    { range: 'light', color: 'monasterio', value: 'chartreuse' },
    { range: '', color: 'chocolate', value: 'chocolate' },
    { range: '', color: 'coral', value: 'coral' },
    { range: '', color: 'azul aciano', value: 'cornflowerblue' },
    { range: 'light', color: 'seda de maiz', value: 'cornsilk' },
    { range: '', color: 'carmesí', value: 'crimson' },
    { range: '', color: 'azul oscuro', value: 'darkblue' },
    { range: '', color: 'cian oscuro', value: 'darkcyan' },
    { range: 'light', color: 'varilla de oro oscura', value: 'darkgoldenrod' },
    { range: 'light', color: 'gris oscuro', value: 'darkgray' },
    { range: '', color: 'verde oscuro', value: 'darkgreen' },
    { range: 'light', color: 'gris oscuro', value: 'darkgrey' },
    { range: 'light', color: 'caqui oscuro', value: 'darkkhaki' },
    { range: '', color: 'darkmagenta', value: 'darkmagenta' },
    { range: '', color: 'verde oliva oscuro', value: 'darkolivegreen' },
    { range: 'light', color: 'naranja oscuro', value: 'darkorange' },
    { range: '', color: 'orquídea oscura', value: 'darkorchid' },
    { range: '', color: 'rojo oscuro', value: 'darkred' },
    { range: 'light', color: 'salmón oscuro', value: 'darksalmon' },
    { range: 'light', color: 'verde oscuro', value: 'darkseagreen' },
    { range: '', color: 'azul oscuro', value: 'darkslateblue' },
    { range: '', color: 'gris oscuro', value: 'darkslategray' },
    { range: '', color: 'darkslategrey', value: 'darkslategrey' },
    { range: 'light', color: 'turquesa oscuro', value: 'darkturquoise' },
    { range: '', color: 'Violeta oscuro', value: 'darkviolet' },
    { range: '', color: 'Rosa profundo', value: 'deeppink' },
    { range: 'light', color: 'Depskyblue', value: 'deepskyblue' },
    { range: '', color: 'dimgray', value: 'dimgray' },
    { range: '', color: 'dimgrey', value: 'dimgrey' },
    { range: '', color: 'dodgerblue', value: 'dodgerblue' },
    { range: '', color: 'ladrillo refractario', value: 'firebrick' },
    { range: 'light', color: 'floral blanco', value: 'floralwhite' },
    { range: '', color: 'bosque verde', value: 'forestgreen' },
    { range: 'light', color: 'Gainsboro', value: 'gainsboro' },
    { range: 'light', color: 'fantasma blanco', value: 'ghostwhite' },
    { range: 'light', color: 'oro', value: 'gold' },
    { range: '', color: 'vara de oro', value: 'goldenrod' },
    { range: 'light', color: 'verde amarillo', value: 'greenyellow' },
    { range: '', color: 'gris', value: 'grey' },
    { range: 'light', color: 'gotas de miel', value: 'honeydew' },
    { range: '', color: 'Rosa caliente', value: 'hotpink' },
    { range: '', color: 'indio rojo', value: 'indianred' },
    { range: '', color: 'índigo', value: 'indigo' },
    { range: 'light', color: 'Marfil', value: 'ivory' },
    { range: 'light', color: 'caqui', value: 'khaki' },
    { range: 'light', color: 'lavanda', value: 'lavender' },
    { range: 'light', color: 'lavanda rubor', value: 'lavenderblush' },
    { range: 'light', color: 'verde césped', value: 'lawngreen' },
    { range: 'light', color: 'gasa de limón', value: 'lemonchiffon' },
    { range: 'light', color: 'azul claro', value: 'lightblue' },
    { range: '', color: 'coral claro', value: 'lightcoral' },
    { range: 'light', color: 'cian claro', value: 'lightcyan' },
    { range: 'light', color: 'dorado claro', value: 'lightgoldenrodyellow' },
    { range: 'light', color: 'gris claro', value: 'lightgray' },
    { range: 'light', color: 'verde claro', value: 'lightgreen' },
    { range: 'light', color: 'gris claro', value: 'lightgrey' },
    { range: 'light', color: 'Rosa claro', value: 'lightpink' },
    { range: 'light', color: 'salmón ligero', value: 'lightsalmon' },
    { range: '', color: 'verde claro', value: 'lightseagreen' },
    { range: 'light', color: 'cielo azul claro', value: 'lightskyblue' },
    { range: '', color: 'luces grises', value: 'lightslategray' },
    { range: '', color: 'luces grises', value: 'lightslategrey' },
    { range: 'light', color: 'azul claro', value: 'lightsteelblue' },
    { range: 'light', color: 'amarillo claro', value: 'lightyellow' },
    { range: 'light', color: 'verde lima', value: 'limegreen' },
    { range: 'light', color: 'lino', value: 'linen' },
    { range: 'light', color: 'mediumaquamarine', value: 'mediumaquamarine' },
    { range: '', color: 'azul medio', value: 'mediumblue' },
    { range: '', color: 'orquídea mediana', value: 'mediumorchid' },
    { range: '', color: 'mediumpurple', value: 'mediumpurple' },
    { range: '', color: 'medio verde', value: 'mediumseagreen' },
    { range: '', color: 'medio pizarra azul', value: 'mediumslateblue' },
    { range: 'light', color: 'medio primaveral', value: 'mediumspringgreen' },
    { range: 'light', color: 'medio turquesa', value: 'mediumturquoise' },
    { range: '', color: 'medio violeta', value: 'mediumvioletred' },
    { range: '', color: 'medianoche azul', value: 'midnightblue' },
    { range: 'light', color: 'crema de menta', value: 'mintcream' },
    { range: 'light', color: 'mistyrose', value: 'mistyrose' },
    { range: 'light', color: 'mocasín', value: 'moccasin' },
    { range: 'light', color: 'navajowhite', value: 'navajowhite' },
    { range: 'light', color: 'Oldlace', value: 'oldlace' },
    { range: '', color: 'verde oliva', value: 'olivedrab' },
    { range: '', color: 'rojo naranja', value: 'orangered' },
    { range: '', color: 'orquídea', value: 'orchid' },
    { range: 'light', color: 'varilla de oro pálido', value: 'palegoldenrod' },
    { range: 'light', color: 'Verde pálido', value: 'palegreen' },
    { range: 'light', color: 'paleturquesa', value: 'paleturquoise' },
    { range: '', color: 'pálido violeta', value: 'palevioletred' },
    { range: 'light', color: 'papaya', value: 'papayawhip' },
    { range: 'light', color: 'melocotón', value: 'peachpuff' },
    { range: '', color: 'Perú', value: 'peru' },
    { range: 'light', color: 'rosado', value: 'pink' },
    { range: 'light', color: 'ciruela', value: 'plum' },
    { range: 'light', color: 'azul pálido', value: 'powderblue' },
    { range: 'light', color: 'marron rosado', value: 'rosybrown' },
    { range: '', color: 'azul real', value: 'royalblue' },
    { range: '', color: 'silla de montar', value: 'saddlebrown' },
    { range: 'light', color: 'salmón', value: 'salmon' },
    { range: 'light', color: 'marrón arenoso', value: 'sandybrown' },
    { range: '', color: 'Mar verde', value: 'seagreen' },
    { range: 'light', color: 'concha', value: 'seashell' },
    { range: '', color: 'tierra de siena', value: 'sienna' },
    { range: 'light', color: 'cielo azul', value: 'skyblue' },
    { range: '', color: 'pizarra azul', value: 'slateblue' },
    { range: '', color: 'gris pizarra', value: 'slategray' },
    { range: '', color: 'pizarra gris', value: 'slategrey' },
    { range: 'light', color: 'nieve', value: 'snow' },
    { range: 'light', color: 'primavera verde', value: 'springgreen' },
    { range: '', color: 'azul acero', value: 'steelblue' },
    { range: 'light', color: 'broncearse', value: 'tan' },
    { range: 'light', color: 'cardo', value: 'thistle' },
    { range: '', color: 'tomate', value: 'tomato' },
    { range: 'light', color: 'turquesa', value: 'turquoise' },
    { range: 'light', color: 'Violeta', value: 'violet' },
    { range: 'light', color: 'trigo', value: 'wheat' },
    { range: 'light', color: 'humo blanco', value: 'whitesmoke' },
    { range: 'light', color: 'amarillo verde', value: 'yellowgreen' },
    { range: '', color: 'Rebeccapurple ', value: 'rebeccapurple' },
  ]
  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
    private svg: Svg
  ) { }

  ngOnInit(): void {
    this.loadSvgImage('zandia');
    // ???? por que selectedColor no se trae los valores directamente del array.?? toco agregarlos aca en el oninit.
    this.selectedColor['color'] = 'blanco';
    this.selectedColor['value'] = 'white';
  }

  showDialog() {
    this.display = true;
  }

  setColor(event): void {
    console.log(event.target.tagName);
    if (event.target.tagName === 'path') event.target.style.fill = this.selectedColor['value'];
  }

  selectColor(event): void {
    console.log('selectColor---->', event);
    this.selectedColor['color'] = event.color;
    this.selectedColor['value'] = event.value;

  }

  eventSidebar(event): void {
    this.sidebarIcon = 'pi pi-chevron-left';
    if (event != 0) {
      this.sidebar = !this.sidebar;
      this.sidebarIcon = 'pi pi-chevron-right';
      this.utilitiesConfig.classLayout = ['cursor-default'];
    } else {
      this.utilitiesConfig.classLayout = [''];
    }

  }

  tooltipColor(event): void {
    const element = document.body.style.setProperty('--tooltip-background', event.value);
    if (event.range === 'light') document.body.style.setProperty('--tooltip-color', 'black');
    else document.body.style.setProperty('--tooltip-color', 'white');
  }

  loadSvgImage(event): void {
    this.loadSvg = this.svg.returnSVG(event);
    this.titleHeader = event == "zandia" ? "sandia" : event;
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }


}
