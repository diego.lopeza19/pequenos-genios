import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, transferArrayItem, CdkDrag, CdkDropList } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';

@Component({
  selector: 'app-sorting-game',
  templateUrl: './sorting-game.component.html',
  styleUrls: ['./sorting-game.component.scss']
})
export class SortingGameComponent implements OnInit {

  display: boolean = true;

  tiles: any = [
    {
      img: "terrestres-acuaticos/rinoceronte.png",
      w: "130px",
      h: "75px",
      id: "tile1",
      correctDiv: "upper"
    },
    {
      img: "terrestres-acuaticos/canguro.png",
      height: "140px",
      id: "tile2",
      w: "75px",
      correctDiv: "upper"

    },
    {
      img: "terrestres-acuaticos/jirafa.png",
      height: "196px",
      id: "tile3",
      correctDiv: "upper",
      w: "75px",

    },
    {
      id: "tileab",
      img: "terrestres-acuaticos/ballena.png",
      w: "125px",
      correctDiv: 'mid'
    },
    {
      id: "tileb",
      img: "terrestres-acuaticos/tiburon.png",
      correctDiv: "mid",
      w: "100px",
    },
    {
      id: "tilec",
      img: "terrestres-acuaticos/pez.png",
      correctDiv: "mid",
      w: "75px"
    }
  ];
  dropzones: any = [
    {
      id: "upper",
      title: "Terrestres - Selva",
      description: "Animales terrestres - Animales de la selva",
      classname: "upper",
      height: "254",
      width: "611",
      img: "terrestres-acuaticos/fondo-terrestre.jpg"
    },
    {
      id: "mid",
      title: "Acuaticos - Granja",
      description: "Animales acuaticos - Animales de la granja",
      classname: "mid",
      height: "254",
      width: "611",
      img: "terrestres-acuaticos/fondo-acuatico.jpg"
    }
  ];
  dropBins: any = {
    'upper': [],
    'mid': [],
    'lower': []
  };


  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
    ) { }

  async ngOnInit() {
    this.utilitiesConfig.classLayout = ['cursor-default']; 
    console.log("->", this.router.url);
    if (this.router.url == "/games/sorting-game-2") {
      this.dropzones[0].img = "selva-granja/fondo-selva.jpg";
      this.dropzones[1].img = "selva-granja/fondo-granja.jpg";
      await this.chargueTiles();
    }
    this.tiles.sort(function () { return Math.random() - 0.5 });
  }

  async chargueTiles() {
    this.tiles = [
      {
        img: "selva-granja/leon.png",
        w: "120px",
        id: "tile1",
        correctDiv: "upper"
      },
      {
        img: "selva-granja/gorila.png",
        id: "tile2",
        w: "75px",
        correctDiv: "upper"
      },
      {
        img: "selva-granja/serpiente.png",
        id: "tile3",
        correctDiv: "upper",
        w: "101px"
      },
      {
        id: "tileab",
        img: "selva-granja/gallina.png",
        w: "125px",
        correctDiv: 'mid'
      },
      {
        id: "tileb",
        img: "selva-granja/caballo.png",
        correctDiv: "mid",
        w: "100px",
      }
      ,
      {
        id: "tilec",
        img: "selva-granja/vaca.png",
        correctDiv: "mid",
        w: "125px",
      }
    ];
  }

  drop(event: CdkDragDrop<string[]>) {
    let item_class = event.item.element.nativeElement.classList;
    let bin_id = event.container.element.nativeElement.id;

    if (!item_class.contains(bin_id)) {
      return;
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

    }
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }
}
