import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadingComponent } from '@reading/main/reading.component';
import { ReadingRoutingModule } from '@reading/reading-routing.module';

@NgModule({
  declarations: [
    ReadingComponent
  ],
  imports: [
    CommonModule,
    ReadingRoutingModule
  ],
})
export class ReadingModule { }
