import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectFinderComponent } from './object-finder.component';

describe('ObjectFinderComponent', () => {
  let component: ObjectFinderComponent;
  let fixture: ComponentFixture<ObjectFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectFinderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
