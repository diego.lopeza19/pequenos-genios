import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DialogModule } from 'primeng/dialog';
import { VideoRoutingModule } from './video-routing.module';
import { VideoComponent } from './video.component';

@NgModule({
  declarations: [
    VideoComponent,
  ],
  imports: [  
    CommonModule,
    DialogModule,  
    VideoRoutingModule
  ],
  exports: [
    VideoComponent,
  ]
})
export class VideoModule { }
