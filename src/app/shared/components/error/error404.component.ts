import { Component } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { Location } from '@angular/common';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component {

  background404: SafeHtml;
  text404: SafeHtml;

  constructor(private _location: Location) {
  }

  ngOnInit() {

  }

  volver() {
    this._location.back();
  }

}
