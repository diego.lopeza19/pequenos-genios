import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';

@Component({
  selector: 'app-simon-says',
  templateUrl: './simon-says.component.html',
  styleUrls: ['./simon-says.component.scss']
})
export class SimonSaysComponent implements OnInit {
  bot = []; //Vector de los botones vacio.
  sec = []; //Vector de la secuencia generada por el programa.
  sec2 = []; //Vector donde se copia la secuencia, para que el usuario valla eliminando los elementos y se de por terminada dicha secuencia y pueda agregar otro elemento a la secuencia "sec".
  puntos = 0; //Puntaje del usuario.
  temp = 0; //Variable para el control del vector sec2[].
  tiempo;
  tiempoColor;
  apagarColor;
  tiempoSec;
  botPos;
  ban = 0;
  tiempoCargar;
  cont = false;
  textButton = "Jugar";
  textIndication;
  display: boolean = true;

  constructor(public utilitiesConfig: UtilitiesConfig, private router: Router) { }

  ngOnInit(): void {
    this.utilitiesConfig.classLayout = ['cursor-default'];
  }

  inicio() {
    this.bot = [];
    this.sec = [];
    this.sec2 = [];
    this.puntos = 0;
    this.temp = 0;
    this.textIndication = "Pon atención ";
    const a = document.getElementsByTagName("article");

    for (let i = 0; i < a.length; i++) {
      this.bot.push(a[i]);//Agregamos los botones al vector.
    }

    this.menu(); //Dispara la funcion menu().
    setTimeout(() => this.cargar(this), 1000); //Dispara la funcion cargar despues de un segundo.
  }

  cargar(ts) {
    var item = ts.bot[Math.floor(Math.random() * ts.bot.length)];//Elemento conseguido al azar del vector bot.
    ts.sec.push(item); //Agregamos el boton elegido al azar en la secuencia.	
    ts.copia(ts); //Dispara la funcion copia
  }

  copia(ts) {
    for (let i = ts.temp; i < ts.sec.length; i++) {
      if (ts.sec[i] != ts.sec2[i]) {
        ts.sec2.push(ts.sec[i]);//Se navega el Vector sec y si su valor es diferente al de la secuencia generada por el programa.												
        ts.colorSec(i); //Dispara la funcion para que encienda el color del boton.
        ts.temp = i; //se guarda el valor de i en la variable temp, asi, se saltea si los elementos en dicha posicion son iguales en ambos vectores.
        break; //Sale de este ciclo.
      }
      if (i >= ts.sec.length - 1) {
        this.textIndication = "Ahora vas tu "
        // ts.tiempo = setTimeout(() => ts.perder(ts), 3000); //Inicia la cuenta del tiempo y si termina, se dispara la funcion perder.			
      }
    }
  }

  colorSec(pos) {
    const i = pos;
    if (this.sec[i].id == "bot1") {
      this.sec[i].style.backgroundColor = "rgba(255, 0, 0, 1)"; //Cambia el color del boton.
      this.sonido("assets/audio/simon/Bot1.mp3"); //Reproduce el audio definido entre parentesis.
    }
    if (this.sec[i].id == "bot2") {
      this.sec[i].style.backgroundColor = "rgba(0, 0, 255, 1)";
      this.sonido("assets/audio/simon/Bot2.mp3");
    }
    if (this.sec[i].id == "bot3") {
      this.sec[i].style.backgroundColor = "rgba(255, 255, 0, 1)";
      this.sonido("assets/audio/simon/Bot3.mp3");
    }
    if (this.sec[i].id == "bot4") {
      this.sec[i].style.backgroundColor = "rgba(0, 255, 0, 1)";
      this.sonido("assets/audio/simon/Bot4.mp3");
    }
    this.tiempoColor = setTimeout(() => this.apagarSec(i, this), 500); //Dispara la funcion apagarSec despues de medio segundo.
  }

  apagarSec(i, ts) {
    ts.sec[i].style.backgroundColor = ""; //Cambia el color del boton a nada, y este toma por consecuencia el definido en el css.
    clearTimeout(ts.tiempoColor); //Sale del bucle de tiempo, sino, seguiria sucediendo.
    ts.tiempoSec = setTimeout(() => ts.copia(ts), 200); //Dispara la funcion copia despes de 1/5 de segundo.
  }

  marcar(event) {
    var acc = window.event || event; //Se guarda el evento o el evento de la ventana.
    var pos = acc.target || acc.srcElement; // Se guarda el objetivo del evento, o la el elemento fuente del evento.
    if (pos.id == "bot1") {
      this.sonido("assets/audio/simon/Bot1.mp3");
      pos.style.backgroundColor = "rgba(255, 0, 0, 1)";
    }
    if (pos.id == "bot2") {
      this.sonido("assets/audio/simon/Bot2.mp3");
      pos.style.backgroundColor = "rgba(0, 0, 255, 1)";
    }
    if (pos.id == "bot3") {
      this.sonido("assets/audio/simon/Bot3.mp3");
      pos.style.backgroundColor = "rgba(255, 255, 0, 1)";
    }
    if (pos.id == "bot4") {
      this.sonido("assets/audio/simon/Bot4.mp3");
      pos.style.backgroundColor = "rgba(0, 255, 0, 1)";
    }
    this.apagarColor = setTimeout(() => this.apagarMarc(pos), 500); //Dispara la funcion apagarMarc despues de medio segundo.
    this.ganar(pos); //Dispara la funcion ganar.
  }

  apagarMarc(pos) {
    this.botPos = pos
    this.botPos.style.backgroundColor = "";
    clearTimeout(this.apagarColor);
  }

  ganar(p) {
    if (this.sec2[0] == p) {
      this.sec2.shift(); //Elimina el primer elemento del vector.
      clearTimeout(this.tiempo);//Anula el tiempo de de la funcion setTimeout.
      this.controlGanar();//Funcion para ver si la secuencia fue terminada o se debe terminar.
    }
    else {
      clearTimeout(this.tiempo);//Anula el tiempo de de la funcion setTimeout.
      this.perder(this); //Dispara la funcion perder.
    }
  }

  controlGanar() {
    if (this.sec2.length == 0) {
      this.puntos += 1; //Aumenta el conteo de puntos del usuario.
      this.temp = 0; //La variable temp, vuelve a 0 para que el control del verctor sec2.
      this.textIndication = "Pon atención ";
      this.tiempoCargar = setTimeout(() => this.cargar(this), 1000); //Dispara la funcion cargar despues de 1 segundo.
    }
    else {
      this.tiempo = setTimeout(this.perder, 3000);//Re-inicia la cuenta del tiempo y si termina, se dispara la funcion perder.
    }
  }

  perder(ts) {
    ts.sonido("assets/audio/simon/Perder.mp3")
    ts.ban = 1; //Cambia el valor de vandera para cambiar el mensaje de menu.
    ts.menu(); //Dispara la funcion menu.
    for (let i = 0; i < 4; i++) {
      if (ts.bot[i].style.backgroundColor != "") {
        ts.apagarMarc(ts.bot[i]); //Apaga los botones que quedaron encendidos. 
      }
    }
    if (ts.tiempo) { clearTimeout(ts.tiempo); }; //Cancela las acciones con cuenta regresiva.
    if (ts.sec.length > 1) { clearTimeout(ts.tiempoColor); };
    if (ts.sec.length > 1) { clearTimeout(ts.tiempoSec); };
    if (ts.sec.length > 1) { clearTimeout(ts.tiempoCargar); };
  }

  menu() {
    if (this.ban == 0) {
      this.cont = true; //Cambia el estilo del contenedor a invisible.
    }
    if (this.ban == 1) {
      this.cont = false; //Cambia el estilo del contenedor a visible.
      this.textButton = "Reiniciar"; //Asigna al boton una nueva palabra dentro.
      this.ban = 0;
    }
  }
  sonido(url) {
    var sonido = new Audio(); //Crea un nuevo objeto audio.
    sonido.src = url; //Le define la url enviada, para poder definir que audio es el que se reproduce.
    sonido.play(); //Se reproduce dicho sonido.
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }

}

