// Angular modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { CharactersComponent } from './components/characters/characters.component';
import { StageComponent } from './components/stage/stage.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { Svg } from './utilities/svg';
import { RouterModule } from '@angular/router';

// Components


@NgModule({
  imports: [
    CommonModule,
    AngularResizedEventModule,
    RouterModule
  ],
  declarations: [
    CharactersComponent,
    StageComponent,
    NavbarComponent,
    LayoutComponent
  ],
  exports: [
    CommonModule,
    StageComponent,
    NavbarComponent,
    LayoutComponent
  ],
  providers: [Svg]
})
export class SharedModule { }
