import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';

@Component({
  selector: 'app-games-menu',
  templateUrl: './games-menu.component.html',
  styleUrls: ['./games-menu.component.sass']
})
export class GamesMenuComponent implements OnInit {

  menuOption: string = '';

  constructor(
    public utilitiesConfig: UtilitiesConfig,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.utilitiesConfig.classLayout = ['cursor-default'];
  }

  goToPage(event): void {
    this.router.navigate([`games/${event}`]);
  }

  goToPrev(): void {
    this.menuOption = '';
  }

  menu(event): void {
    this.menuOption = event;
  }

}
