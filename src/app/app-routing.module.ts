import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error404Component } from '@shared/components/error/error404.component';
import { LayoutComponent } from '@shared/layout/layout.component';

const ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'games'},
  {
    path: '',
    component: LayoutComponent,
    children: [      
      {
        path: 'reading',        
        loadChildren: (): Promise<any> =>
          import('@reading/reading.module').then((m) => m.ReadingModule),
      },
      {
        path: 'games',        
        loadChildren: (): Promise<any> =>
          import('@games/games.module').then((m) => m.GamesModule),
      },
      {
        path: 'video',        
        loadChildren: (): Promise<any> =>
          import('@video/video.module').then((m) => m.VideoModule),
      }
    ],
  },
  { path: '404', component: Error404Component },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
