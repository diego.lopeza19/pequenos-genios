import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class UtilitiesConfig {

  public classLayout: any[] = [''];
  

  public optionsDialog = {
    modal: true,
    styleClass: 'p-col-12 sm:col-12 md:col-10 lg:col-9 xl:col-8 board',
    position: 'top',
    breakpoints: {
      '1400px': '70vw',
      '1200px': '75vw',
      '992px': '85vw',
      '768px': '80vw',
      '576px': '85vw',
      '460px': '90vw',
      '360px': '100vw',
    },
    baseZIndex: 0,
    draggable: false,
    maximizable: false,
    responsive: true,
    style: { '': '' },
    contentStyle: { '': '' },
  };

  customCursor():void {

  }

  public posX: any;
  public posY: any;
  

  sigueme(event: any): void { 
    this.posX = event.x;
    this.posY = event.y;
    // this.posY = event.layerY;
  //   var x = window.event.x + document.body.scrollLeft;
  // var y = window.event.y + document.body.scrollTop;

  // document.getElementById("siguelo").style.left = x + "px";
  // document.getElementById("siguelo").style.top = y + "px";
  }  
  
}
