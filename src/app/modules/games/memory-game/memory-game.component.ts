import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';
import { CardData } from './card-memory/card-data.model';

@Component({
  selector: 'app-memory-game',
  templateUrl: './memory-game.component.html',
  styleUrls: ['./memory-game.component.scss']
})
export class MemoryGameComponent implements OnInit {

  cardImages = [
    'ojo',
    'oreja',
    'boca'
  ];

  level = 0;
  tittle = "partes del cuerpo"

  levels = [
    'cabeza',
    'nariz',
    'pie',
    'mano'
  ];

  cards: CardData[] = [];

  flippedCards: CardData[] = [];

  matchedCount = 0;
  display: boolean = true;

  shuffleArray(anArray: any[]): any[] {
    return anArray.map(a => [Math.random(), a])
      .sort((a, b) => a[0] - b[0])
      .map(a => a[1]);
  }

  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
  ) { }

  ngOnInit(): void {
    this.utilitiesConfig.classLayout = ['cursor-default'];
    if (this.router.url == "/games/memory-1") {
      this.tittle = "medios de transporte";
      this.cardImages = ['bici', 'barco', 'bus', 'tren'];
      this.levels = ['taxi', 'carro', 'moto', 'avion'];
    }
    this.setupCards();
  }

  setupCards(): void {
    this.cards = [];
    this.cardImages.forEach((image) => {
      const cardData: CardData = {
        imageId: image,
        state: 'default'
      };

      this.cards.push({ ...cardData });
      this.cards.push({ ...cardData });

    });

    this.cards = this.shuffleArray(this.cards);
  }

  cardClicked(index: number): void {
    const cardInfo = this.cards[index];

    if (cardInfo.state === 'default' && this.flippedCards.length < 2) {
      cardInfo.state = 'flipped';
      this.flippedCards.push(cardInfo);

      if (this.flippedCards.length > 1) {
        this.checkForCardMatch();
      }

    } else if (cardInfo.state === 'flipped') {
      cardInfo.state = 'default';
      this.flippedCards.pop();

    }
  }

  checkForCardMatch(): void {
    setTimeout(() => {
      const cardOne = this.flippedCards[0];
      const cardTwo = this.flippedCards[1];
      const nextState = cardOne.imageId === cardTwo.imageId ? 'matched' : 'default';
      cardOne.state = cardTwo.state = nextState;

      this.flippedCards = [];

      if (nextState === 'matched') {
        this.matchedCount++;

        if (this.matchedCount === this.cardImages.length) {
          alert("FELICIDADES ENCONTRASTE TODO");
          this.restart();
        }
      }

    }, 1000);
  }

  restart(): void {
    if (this.level < 4) {
      this.cardImages.push(this.levels[this.level]);
      this.level += 1;
    } else {
      this.level = 0;
      this.cardImages = this.cardImages.slice(0, 3);
    }
    this.matchedCount = 0;
    this.setupCards();
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }

}
