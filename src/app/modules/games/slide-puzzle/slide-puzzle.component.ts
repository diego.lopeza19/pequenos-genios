import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';

@Component({
  selector: 'app-slide-puzzle',
  templateUrl: './slide-puzzle.component.html',
  styleUrls: ['./slide-puzzle.component.scss']
})
export class SlidePuzzleComponent implements OnInit {

  // Arreglo que contiene las intrucciones del juego
  instrucciones = [
    "Utiliza las flechas del teclado para mover las piezas",
    "Debes lograr formar la imagen objetivo",
    "Una vez logrado ganas la partida",
  ];
  movimientos = [];
  grilla = [];
  grillaGanadora = [];
  filaVacia;
  columnaVacia;
  ultimoMov = "";
  selected = "bmo"
  options = ["bmo", "picachu"]
  restart = true;
  display: boolean = true;


  /* codigosDireccion es un objeto que te permite reemplazar
  el uso de números confusos en tu código. Para referirte a la dir
  izquierda, en vez de usar el número 37, ahora podés usar:
  codigosDireccion.IZQUIERDA. Esto facilita mucho la lectura del código. */

  codigosDireccion = {
    IZQUIERDA: 37,
    ARRIBA: 38,
    DERECHA: 39,
    ABAJO: 40,
  };

  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
  ) { }

  /* Se inicia el rompecabezas mezclando las piezas 60 veces 
  y ejecutando la función para que se capturen las teclas que 
  presiona el usuario */

  // Ejecutamos la función iniciar
  ngOnInit(): void {
    this.utilitiesConfig.classLayout = ['cursor-default'];
    if (this.router.url == "/games/puzzle-2") {
      this.selected = "cuarto"
      this.options = ["baño", "cuarto","sala","cocina"]
    }
    setTimeout(() => {
      this.mostrarInstrucciones(this.instrucciones);
      this.capturarTeclas();
      this.initGame();
    }, 500);
  }

  initGame() {
    // Arreglo para ir guardando los movimientos que se vayan realizando
    this.movimientos = [];

    // Representación de la grilla. Cada número representa a una pieza.
    // El 9 es la posición vacía
    this.grilla = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ];
    // Grilla que me sirve para chequear si gane en la funcion chequearSiGano()
    this.grillaGanadora = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ];

    /* Estas dos variables son para guardar la posición de la pieza vacía. 
    Esta posición comienza siendo la [2, 2]*/
    this.filaVacia = 2;
    this.columnaVacia = 2;

    this.mezclarPiezas(50);
  }

  /* Esta función deberá recorrer el arreglo de instrucciones pasado por parámetro. 
  Cada elemento de este arreglo deberá ser mostrado en la lista con id 'lista-instrucciones'. 
  Para eso deberás usar la función ya implementada mostrarInstruccionEnLista().
  Podés ver su implementación en la ultima parte de este codigo. */
  mostrarInstrucciones(instrucciones) {
    //COMPLETAR
    for (var i = 0; i < instrucciones.length; i++) {
      this.mostrarInstruccionEnLista(instrucciones[i], "lista-instrucciones");
    }
  }

  /* COMPLETAR: Crear función que agregue la última dirección al arreglo de movimientos
  y utilice actualizarUltimoMovimiento para mostrarlo en pantalla */
  agregarUltimoMovimiento(direccion) {
    this.movimientos.push(direccion);
    this.actualizarUltimoMovimiento(direccion);
  }

  /* Esta función va a chequear si el Rompecabezas esta en la posicion ganadora. 
  Existen diferentes formas de hacer este chequeo a partir de la grilla. */
  chequearSiGano() {
    //COMPLETAR
    for (var i = 0; i < this.grilla.length; i++) {
      for (var j = 0; j < this.grilla[i].length; j++) {
        if (this.grilla[i][j] !== this.grillaGanadora[i][j]) {
          return false;
        }
      }
    }
    return true;
  }

  // Implementar alguna forma de mostrar un cartel que avise que ganaste el juego
  mostrarCartelGanador() {
    //COMPLETAR
    if (this.chequearSiGano() === true) {
      alert("GANASTE!");
    }
  }

  // Funcion para refrescar la pantalla
  refrescar() {
    this.initGame();
  }

  /* Función que intercambia dos posiciones en la grilla.
  Pensar como intercambiar dos posiciones en un arreglo de arreglos. 
  Para que tengas en cuenta:
  Si queremos intercambiar las posiciones [1,2] con la [0, 0], si hacemos: 
  arreglo[1][2] = arreglo[0][0];
  arreglo[0][0] = arreglo[1][2];
  
  En vez de intercambiar esos valores vamos a terminar teniendo en ambas posiciones el mismo valor.
  Se te ocurre cómo solucionar esto con una variable temporal?
  */
  intercambiarPosicionesGrilla(
    filaPos1,
    columnaPos1,
    filaPos2,
    columnaPos2
  ) {
    //COMPLETAR
    let temporal = this.grilla[filaPos1][columnaPos1];
    this.grilla[filaPos1][columnaPos1] = this.grilla[filaPos2][columnaPos2];
    this.grilla[filaPos2][columnaPos2] = temporal;
  }

  // Actualiza la posición de la pieza vacía
  actualizarPosicionVacia(nuevaFila, nuevaColumna) {
    //COMPLETAR
    this.filaVacia = nuevaFila;
    this.columnaVacia = nuevaColumna;
  }

  // Para chequear si la posicón está dentro de la grilla.
  posicionValida(fila, columna) {
    //COMPLETAR
    if (fila < 0 || fila > 2 || columna < 0 || columna > 2) {
      return false;
    } else {
      return true;
    }
  }

  /* Movimiento de fichas, en este caso la que se mueve es la blanca intercambiando su posición con otro elemento.
  Las direcciones están dadas por números que representa: arriba (38), abajo (40), izquierda (37), derecha (39) */
  moverEnDireccion(direccion) {
    var nuevaFilaPiezaVacia;
    var nuevaColumnaPiezaVacia;

    // Mueve pieza hacia la abajo, reemplazandola con la blanca
    if (direccion === this.codigosDireccion.ABAJO) {
      nuevaFilaPiezaVacia = this.filaVacia + 1;
      nuevaColumnaPiezaVacia = this.columnaVacia;
    }

    // Mueve pieza hacia arriba, reemplazandola con la blanca
    else if (direccion === this.codigosDireccion.ARRIBA) {
      nuevaFilaPiezaVacia = this.filaVacia - 1;
      nuevaColumnaPiezaVacia = this.columnaVacia;
    }

    // Mueve pieza hacia la derecha, reemplazandola con la blanca
    else if (direccion === this.codigosDireccion.DERECHA) {
      //COMPLETAR
      nuevaFilaPiezaVacia = this.filaVacia;
      nuevaColumnaPiezaVacia = this.columnaVacia + 1;
    }

    // Mueve pieza hacia la izquierda, reemplazandola con la blanca
    else if (direccion === this.codigosDireccion.IZQUIERDA) {
      // COMPLETAR
      nuevaFilaPiezaVacia = this.filaVacia;
      nuevaColumnaPiezaVacia = this.columnaVacia - 1;
    }

    /* A continuación se chequea si la nueva posición es válida, si lo es, se intercambia. 
    Para que esta parte del código funcione correctamente deberás haber implementado 
    las funciones posicionValida, intercambiarPosicionesGrilla y actualizarPosicionVacia */

    if (this.posicionValida(nuevaFilaPiezaVacia, nuevaColumnaPiezaVacia)) {
      this.intercambiarPosiciones(
        this.filaVacia,
        this.columnaVacia,
        nuevaFilaPiezaVacia,
        nuevaColumnaPiezaVacia
      );
      this.actualizarPosicionVacia(nuevaFilaPiezaVacia, nuevaColumnaPiezaVacia);

      //COMPLETAR: Agregar la dirección del movimiento al arreglo de movimientos
      this.agregarUltimoMovimiento(direccion);
    }
  }

  //////////////////////////////////////////////////////////
  ////////A CONTINUACIÓN FUNCIONES YA IMPLEMENTADAS.////////
  /////////NO TOCAR A MENOS QUE SEPAS LO QUE HACES//////////
  //////////////////////////////////////////////////////////

  /* Las funciones y variables que se encuentran a continuación ya están implementadas.
  No hace falta que entiendas exactamente que es lo que hacen, ya que contienen
  temas aún no vistos. De todas formas, cada una de ellas tiene un comentario
  para que sepas que se está haciendo a grandes rasgos. NO LAS MODIFIQUES a menos que
  entiendas perfectamente lo que estás haciendo! */

  /* Funcion que realiza el intercambio logico (en la grilla) y ademas actualiza
  el intercambio en la pantalla (DOM). Para que funcione debera estar implementada
  la funcion intercambiarPosicionesGrilla() */
  intercambiarPosiciones(fila1, columna1, fila2, columna2) {
    // Intercambio posiciones en la grilla
    let pieza1 = this.grilla[fila1][columna1];
    let pieza2 = this.grilla[fila2][columna2];

    this.intercambiarPosicionesGrilla(fila1, columna1, fila2, columna2);
    this.intercambiarPosicionesDOM("pieza" + pieza1, "pieza" + pieza2);
  }

  /* Intercambio de posiciones de los elementos del DOM que representan
  las fichas en la pantalla */

  intercambiarPosicionesDOM(idPieza1, idPieza2) {
    // Intercambio posiciones en el DOM
    var elementoPieza1 = document.getElementById(idPieza1);
    var elementoPieza2 = document.getElementById(idPieza2);

    var padre = elementoPieza1.parentNode;

    var clonElemento1 = elementoPieza1.cloneNode(true);
    var clonElemento2 = elementoPieza2.cloneNode(true);

    padre.replaceChild(clonElemento1, elementoPieza2);
    padre.replaceChild(clonElemento2, elementoPieza1);
  }

  /* Actualiza la representación visual del último movimiento 
  en la pantalla, representado con una flecha. */
  actualizarUltimoMovimiento(direccion) {
    switch (direccion) {
      case this.codigosDireccion.ARRIBA:
        this.ultimoMov = "↑";
        break;
      case this.codigosDireccion.ABAJO:
        this.ultimoMov = "↓";
        break;
      case this.codigosDireccion.DERECHA:
        this.ultimoMov = "→";
        break;
      case this.codigosDireccion.IZQUIERDA:
        this.ultimoMov = "←";
        break;
    }
  }

  /* Esta función permite agregar una instrucción a la lista
  con idLista. Se crea un elemento li dinámicamente con el texto 
  pasado con el parámetro "instrucción". */
  mostrarInstruccionEnLista(instruccion, idLista) {
    let ul = document.getElementById(idLista);
    let li = document.createElement("li");
    li.textContent = instruccion;
    ul.appendChild(li);
  }

  /* Función que mezcla las piezas del tablero una cantidad de veces dada.
  Se calcula una posición aleatoria y se mueve en esa dirección. De esta forma
  se mezclará todo el tablero. */

  mezclarPiezas(veces) {
    if (veces <= 0) {
      return;
    }

    var direcciones = [
      this.codigosDireccion.ABAJO,
      this.codigosDireccion.ARRIBA,
      this.codigosDireccion.DERECHA,
      this.codigosDireccion.IZQUIERDA,
    ];

    var direccion = direcciones[Math.floor(Math.random() * direcciones.length)];
    this.moverEnDireccion(direccion);
    const tss = this;
    setTimeout(function () {
      tss.mezclarPiezas(veces - 1);
    }, 100);
  }

  /* capturarTeclas: Esta función captura las teclas presionadas por el usuario. Javascript
  permite detectar eventos, por ejemplo, cuando una tecla es presionada y en 
  base a eso hacer algo. No es necesario que entiendas como funciona esto ahora, 
  en el futuro ya lo vas a aprender. Por ahora, sólo hay que entender que cuando
  se toca una tecla se hace algo en respuesta, en este caso, un movimiento */
  capturarTeclas() {
    const tss = this;
    document.body.onkeydown = function (evento) {
      console.log(evento);

      if (
        evento.which === tss.codigosDireccion.ABAJO ||
        evento.which === tss.codigosDireccion.ARRIBA ||
        evento.which === tss.codigosDireccion.DERECHA ||
        evento.which === tss.codigosDireccion.IZQUIERDA
      ) {
        tss.moverEnDireccion(evento.which);

        var gano = tss.chequearSiGano();
        if (gano) {
          setTimeout(function () {
            tss.mostrarCartelGanador();
          }, 500);
        }
        evento.preventDefault();
      }
    };
  }

  selectedOption(option) {
    this.restart = false;
    if (option) this.selected = option;

    setTimeout(() => {
      this.restart = true;
    }, 500);
    setTimeout(() => {
      this.refrescar();
    }, 1000);
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }
}
