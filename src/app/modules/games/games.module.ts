import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Modules
import { SharedModule } from '@shared/shared.module';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { TooltipModule } from 'primeng/tooltip';
//components
import { GamesRoutingModule } from '@games/games-routing.module';
import { SlidePuzzleComponent } from '@games/slide-puzzle/slide-puzzle.component';
import { ButtonModule } from 'primeng/button';
import { SortingGameComponent } from './sorting-game/sorting-game.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GamesMenuComponent } from './games-menu/games-menu.component';
import { PaintComponent } from './paint/paint.component';
import { MemoryGameComponent } from './memory-game/memory-game.component';
import { CardMemoryComponent } from './memory-game/card-memory/card-memory.component';
import { ObjectFinderComponent } from './object-finder/object-finder.component';
import { SimonSaysComponent } from './simon-says/simon-says.component';
import { GameBoardComponent } from '@games/game-board/game-board.component';


@NgModule({
  declarations: [PaintComponent, SlidePuzzleComponent, SortingGameComponent, GamesMenuComponent, MemoryGameComponent, CardMemoryComponent, ObjectFinderComponent, SimonSaysComponent, GameBoardComponent],
  imports: [
    CommonModule,
    GamesRoutingModule,
    SharedModule,
    DialogModule,
    SidebarModule,
    ButtonModule,
    TooltipModule,
    DragDropModule
  ],
  exports: [SlidePuzzleComponent, SortingGameComponent, MemoryGameComponent, CardMemoryComponent, PaintComponent, GameBoardComponent]
})
export class GamesModule { }
