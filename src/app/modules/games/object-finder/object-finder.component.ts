import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UtilitiesConfig } from '@core/services/utilities-config.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-object-finder',
  templateUrl: './object-finder.component.html',
  styleUrls: ['./object-finder.component.scss']
})
export class ObjectFinderComponent implements OnInit {

  imagenPrin = [['01.png', '02.png', '03.png', '04.png', '05.png', '06.png',
    '07.png', '08.png', '09.png', '10.png', '11.png', '12.png', '13.png',
    '14.png', '15.png', '16.png', '17.png', '18.png', '19.png', '20.png'],
    'avisual1.jpg', 'avisual2.jpg', 'avisual3.jpg', 'avisual4.jpg', 'avisual5.jpg',
    'avisual6.jpg', 'avisual7.jpg', 'avisual8.jpg', 'avisual9.jpg', 'avisual10.jpg',
    'avisual11.jpg', 'avisual12.jpg', 'avisual13.jpg', 'avisual14.jpg', 'avisual15.jpg',
    'avisual16.jpg', 'avisual17.jpg', 'avisual18.jpg', 'avisual19.jpg', 'avisual20.jpg',
    'avisual.jpg'];

  buscar = "";
  posicion;
  display: boolean = true;
  cont = 19;
  mapSrc: any = "avisual20.jpg";
  @ViewChild('map') desiredElement: ElementRef;
  constructor(public utilitiesConfig: UtilitiesConfig, private router: Router, private changeDetector : ChangeDetectorRef) { }

  ngOnInit(): void {
    this.changeDetector.detectChanges();
    this.utilitiesConfig.classLayout = ['cursor-default'];
    this.buscar = this.imagenPrin[0][this.cont];
    // do stuff with nativeElement
    this.posicion = this.desiredElement.nativeElement.getBoundingClientRect(); // <== works!
    console.log(this.posicion, "<...");
  }

  getClick(e) {
    let tss = this;
    console.log(tss.posicion);
    console.log(`top ${tss.posicion.top}, posicion.right, posicion.bottom, left ${tss.posicion.left}`); // coordinadas de imagen  
    let x = Math.floor(e.clientX - tss.posicion.left);
    let y = Math.floor(e.clientY - tss.posicion.top);

    console.log(e);
    console.log( `x = ${Math.floor(e.clientX - tss.posicion.left)}, y = ${Math.floor(e.clientY - tss.posicion.top)}`); //coordinadas del cursor

    switch (true) {
      case ((270 <= x && x <= 283) && (448 <= y && y <= 472) && tss.cont === 19):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((737 <= x && x <= 752) && (147 <= y && y <= 170) && tss.cont === 18):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((270 <= x && x <= 286) && (128 <= y && y <= 155) && tss.cont === 17):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((576 <= x && x <= 635) && (286 <= y && y <= 309) && tss.cont === 16):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((649 <= x && x <= 670) && (133 <= y && y <= 153) && tss.cont === 15):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((((785 <= x && x <= 795) && (461 <= y && y <= 471)) ||
        ((320 <= x && x <= 337) && (177 <= y && y <= 187)) ||
        ((181 <= x && x <= 195) && (229 <= y && y <= 237))) && tss.cont === 14):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((280 <= x && x <= 324) && (383 <= y && y <= 409) && tss.cont === 13):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((293 <= x && x <= 314) && (74 <= y && y <= 90) && tss.cont === 12):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((449 <= x && x <= 468) && (144 <= y && y <= 165) && tss.cont === 11):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((674 <= x && x <= 714) && (168 <= y && y <= 213) && tss.cont === 10):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((179 <= x && x <= 201) && (266 <= y && y <= 298) && tss.cont === 9):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((514 <= x && x <= 532) && (343 <= y && y <= 386) && tss.cont === 8):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((202 <= x && x <= 234) && (483 <= y && y <= 516) && tss.cont === 7):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((351 <= x && x <= 387) && (436 <= y && y <= 481) && tss.cont === 6):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((54 <= x && x <= 71) && (130 <= y && y <= 157) && tss.cont === 5):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((173 <= x && x <= 198) && (187 <= y && y <= 208) && tss.cont === 4):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((230 <= x && x <= 257) && (196 <= y && y <= 215) && tss.cont === 3):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((490 <= x && x <= 510) && (298 <= y && y <= 314) && tss.cont === 2):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((769 <= x && x <= 791) && (315 <= y && y <= 340) && tss.cont === 1):
        tss.cont--;
        tss.cambiador(tss.cont);
        break;
      case ((456 <= x && x <= 480) && (163 <= y && y <= 189) && tss.cont === 0):
        tss.mapSrc = this.imagenPrin[21];
        document.getElementById("text").innerHTML = `<div align=center>¡¡Has logrado terminar el juego!! ¡¡Felicitaciones!!</div>`;
    }



  }

  cambiador(cont) {
    this.buscar = this.imagenPrin[0][cont];
    this.mapSrc = this.imagenPrin[cont + 1];
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }

}
