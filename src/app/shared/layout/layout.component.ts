import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesConfig } from '@core/services/utilities-config.service';
import { Svg } from '@shared/utilities/svg';
import { ResizedEvent } from 'angular-resize-event';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit, OnChanges {

  resizedLayout: string = '';
  screen: string;

  pig: any;
  ruta:string = '';

  constructor(
    private router: Router,
    public utilitiesConfig: UtilitiesConfig,
    private svg: Svg
  ) { }

  ngOnChanges(): void {
    this.ruta = this.router.url;
    console.log(this.ruta);
    
  }

  ngOnInit(): void {    
    this.utilitiesConfig.classLayout = [''];
    this.pig = this.svg.returnSVG('pig');
  }

  onResizedLayout(event: ResizedEvent): void {         
    if(event.newWidth <= 896){
      this.Responsive(event.newWidth);
      if(this.resizedLayout === 'mobile')return;
      this.resizedLayout = `mobile`;
    }else{
      this.Responsive(event.newWidth);
      if(this.resizedLayout != 'mobile')return;
      this.resizedLayout = ``;
    }
  }

  //establece en el elemnto padre html la clase que identifica el ancho de pantalla con el fin de evitar generar mediaqueries
  Responsive(event) {
    if(event >= 1400)this.screen = 'xxl';
    else if(event >= 1200)this.screen = 'xl';
    else if(event >= 992)this.screen = 'lg';
    else if(event >= 768)this.screen = 'md';
    else if(event >= 576)this.screen = 'sm';
    else if(event <= 575)this.screen = 'xs';    
  }

  goToPage(): void {
    this.router.navigate([`games`]);
  }

  onActivate():void {
    this.ruta = this.router.url;
    console.log(this.ruta);
  }

  onDeactivate(): void {

  }

}
