import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidePuzzleComponent } from '@games/slide-puzzle/slide-puzzle.component';
import { SortingGameComponent } from '@games/sorting-game/sorting-game.component';
import { MemoryGameComponent } from '@games/memory-game/memory-game.component';
import { GamesMenuComponent } from '@games/games-menu/games-menu.component';
import { ObjectFinderComponent } from '@games/object-finder/object-finder.component';
import { PaintComponent } from '@games/paint/paint.component';
import { SimonSaysComponent } from '@games/simon-says/simon-says.component';
import { GameBoardComponent } from './game-board/game-board.component';
import { VideoComponent } from '@video/video.component';

const ROUTES: Routes = [
  { path: '', component: GamesMenuComponent },
  { path: 'paint', component: PaintComponent },
  { path: 'puzzle-1', component: SlidePuzzleComponent },
  { path: 'puzzle-2', component: SlidePuzzleComponent },
  { path: 'sorting-game-1', component: SortingGameComponent },
  { path: 'sorting-game-2', component: SortingGameComponent },
  { path: 'object-finder', component: ObjectFinderComponent },
  { path: 'memory', component: MemoryGameComponent },
  { path: 'memory-1', component: MemoryGameComponent },
  { path: 'simon-say', component: SimonSaysComponent },
  { path: 'snake', component: GameBoardComponent },
  { path: 'patito-feo', component: VideoComponent },
  { path: 'rapunzel', component: VideoComponent },
  { path: 'pinocho', component: VideoComponent },
  { path: 'blanca-nieves', component: VideoComponent },
  { path: 'caperucita-roja', component: VideoComponent },
  { path: 'cerditos', component: VideoComponent },
  { path: 'cenicienta', component: VideoComponent },
  { path: 'gato-con-botas', component: VideoComponent },
  { path: 'liebre-tortuga', component: VideoComponent },
  { path: 'raton-leon', component: VideoComponent },

  { path: 'dientes', component: VideoComponent },
  { path: 'sapito', component: VideoComponent },
  { path: 'auto-papa', component: VideoComponent },
  { path: 'taza', component: VideoComponent },
  { path: 'estrellita', component: VideoComponent },
  { path: 'asereje', component: VideoComponent },
  { path: 'aplaudir', component: VideoComponent },
  { path: 'animales', component: VideoComponent },
  { path: 'pin-pon', component: VideoComponent },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(ROUTES),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})
export class GamesRoutingModule { }
